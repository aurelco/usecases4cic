

## Steps for setup

Cleaning the environment :
- rm -rf ./persistence/grafana/*
- rm -rf ./persistence/influxdb/*
- chmod -R a+w ./persistence/grafana

Influxdb - Configuration
- Network port : in ./compose.yml : ports : "8086:8086"
- Databases creation : in ./initialize/influxdb-init.sh
- Users admin + writers + readers : in ./initialize/influxdb-init.sh


Grafana - Configuration
- Datasource Influxdb : in ./grafana/provisioning/datasources/influxdb.yml
- Network port for clients : ./grafana/grafana.ini
- Certificates :
    - put cert file in ./grafana/cert
    - modify https and path to cert file in ./grafana/grafana.ini
- User admin : ./grafana/grafana.ini
- Users writers & readers : to be added through grafana admin account



CMD sleep infinity
docker exec -ti 6bad35fa9a25 /bin/bash