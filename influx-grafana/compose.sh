#!/bin/bash
action=$1

export COMPOSE_PROJECT_NAME=projectname
export COMPOSE_FILE=compose.yml
# export COMPOSE_FILE=db-postgresql.yml:coins-monitor.yml
# export HOST_IP=`ip -4 addr show scope global dev docker0 | grep inet | awk '{print \$2}' | cut -d / -f 1`


if [ "$action" == "up" ]; then

    # add -d to detach and get back the prompt --> use ./compose.sh down to stop
	docker-compose up --build --remove-orphans -d

elif [ "$action" == "down" ]; then

	docker-compose down --remove-orphans

else

	echo "Unknown action"
	exit 1

fi
