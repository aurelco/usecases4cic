#!/bin/bash
# ----------------------------------------------------------------
# Appli Launcher : Version-181211
# -----
# Script to manage status / start / stop / restart for a list of applications (python scripts for instance).
# Supports multi processing : 1 apps may have several PIDs.
# Uses SIGTERM, wait and them use SIGKILL to stop apps.
# -----
# How-to :
#  - Complete the 4 arrays with the parameters of your apps.
#  - By launching the script, possible to start stop restart status apps.
# -----
# Notes :
#  - BASH IS MANDATORY : array not compatible with sh : #!/bin/sh
#  - shell guide : https://openclassrooms.com/courses/reprenez-le-controle-a-l-aide-de-linux/afficher-et-manipuler-des-variables
# ------------------------------------------------------------------

# --------------- PARAMETERS : 1 APP PER COLUMN ---------------
APPS_NAM=( 'mqtt-processor-safr'    )  # The names shown on screen
APPS_DIR=( './'                     )  # The dir (a cd will be done before launch)
APPS_CMD=( 'mqtt-processor-safr.py' )  # The command that will be executed
APPS_TYP=( 'python3'                )  # "python3" or "command", will be launch with a nohup for instance
# -------------------------------------------

echo ""
ORDRE=$1
if [ "$1" != "stop" ] && [ "$1" != "start" ] && [ "$1" != "status" ] && [ "$1" != "restart" ]; then
    echo "Usage : $0 status|start|stop|restart"
    echo ""
	ORDRE="status"
fi

#----- Function : stopWaitKillPerName thePID
stopWaitKillPerPID() {
    echo -n "Sending SIGTERM.."
    kill -15 ${1}
    local count=0
    local stopped=0
    until [ ${count} -gt 30 ] ; do
        ps -p "${1}" > /dev/null
        if [ $? -ne 0 ] ; then
          stopped=1
          break
        fi
        echo -n '.'
        sleep 1
        count=$((count+1))
    done
    if [ ${stopped} = 0 ]; then
        echo -n " Sending SIGKILL.."
        kill -9 ${1}
        echo " Stopped OK (not gratefully)"
    else
        echo " Stopped OK"
    fi
}

# ------ Looping on the apps : 1 app can have several instances with PIDs
app_i=0
while [ "${APPS_NAM[$app_i]}" != '' ]; do
    # --- Here we have the matching name of the process, can have several PIDs
    echo ""
    appNam=${APPS_NAM[$app_i]}
    appDir=${APPS_DIR[$app_i]}
    appCmd=${APPS_CMD[$app_i]}
    appTyp=${APPS_TYP[$app_i]}
    appRunning=0
    let app_i=app_i+1

    # recup d'un array de PIDs
    PIDs4app=($(pgrep -f "${appCmd}"))

    # --- Browsing the PIDs for the ProcessName : print status ON + stop (for stop or restart)
    app_pidi=0
    while [ "${PIDs4app[app_pidi]}" != '' ]; do
        # --- Here PID per ID for a same processName
        lePID=${PIDs4app[app_pidi]}
        let app_pidi=app_pidi+1
        appRunning=1

        # -------------- STATUS -----------
        if [ "$ORDRE" = 'status' ] ; then
            echo "${appNam}  - Running OK, PID=${lePID}"

        fi

        # -------------- STOP + RESTART -----------
        if [ "$ORDRE" = 'restart' ] || [ "$ORDRE" = 'stop' ] ; then
            echo -n "${appNam}  - PID=${lePID}  "
            stopWaitKillPerPID ${lePID}
            appRunning=0
        fi

    done

    # ----------- NO PID detected : not running
    if [ ${appRunning} -eq 0 ]; then
        # -------------- STATUS + STOP + RESTART : on loggue que not running -----------
        if [ "$ORDRE" = 'status' ] || [ "$ORDRE" = 'restart' ] || [ "$ORDRE" = 'stop' ] ; then
            echo "${appNam}  - Not Running"
        fi

        # -------------- START + RESTART : starting -----------
        if [ "$ORDRE" = 'start' ] || [ "$ORDRE" = 'restart' ] ; then
            echo -n "${appNam}  - "
            if [ "$appTyp" = 'python3' ] ; then
                echo -n "Starting Python3 nohup... "
                cd ${appDir}
                nohup python3 ${appCmd} >/dev/null 2>&1 &
                echo "Started OK"
            elif [ "$appTyp" = 'command' ] ; then
                echo -n "Starting nohup... "
                cd ${appDir}
                nohup ${appCmd} >/dev/null 2>&1 &
                echo "Started OK"
             else
                echo "Error : Launcher unknown.."
            fi
        fi
    # ----------- Some PID are running
    else
        # -------------- START + RESTART : logging that already running -----------
        if [ "$ORDRE" = 'start' ] || [ "$ORDRE" = 'restart' ] ; then
            echo "${appNam}  - Already running OK"
        fi
    fi
done

echo ""
echo ""

