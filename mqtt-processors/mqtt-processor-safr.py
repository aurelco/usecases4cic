# coding=utf-8
__version__ = '190917'

import logging
import sys
import pyclasses.TomlConfig
import pyclasses.MqttTLS
import pyclasses.Functions
import re


if __name__ == '__main__':
    logging.info("Starting")

    # ------------ Getting config ------------
    try :
        conf_obj = pyclasses.TomlConfig.TomlConfig(None)
        conf_obj.prompt_for_values(propose_pickle=True)
        conf     = conf_obj.conf

        # assign global variables
        topic_source = conf['topics']['topic_source']
        if len(topic_source) == 0 :
            raise Exception('Source topic undefined')
        topic_destination = conf['topics']['topic_destination']
        if len(topic_destination) == 0 :
            raise Exception('Destination topic undefined')

        mqc = pyclasses.MqttTLS.MQTTConnector(config=conf['mqttbroker'])
    except Exception as CEX :
        logging.critical("Exception with config file, exiting : {:s}".format(str(CEX)))
        sys.exit(-1)

    # ------------ MQTT Callbacks definition ------------
    def on_connect(client, userdata, flags, rc):
        logging.warning("MQTT Connection, subscription to topic(s)")
        mqc.mqttClient.subscribe(topic=topic_source)

    def on_disconnect(client, userdata, rc) :
        logging.warning("MQTT Disconnected")

    def on_message(client, userdata, msg):
        try :
            latopic   = str(msg.topic)
            lepayload = bytes.decode(msg.payload)
            [source_name, person_name, gender, age, sentiment, smile, similarity_score] = lepayload.split('/')
            source_name      = re.sub(r'[^A-Za-z0-9_]', '', source_name)
            person_name      = re.sub(r'[^A-Za-z0-9_]', '', person_name)
        except Exception as EE :
            logging.warning("Cannot extract payload : {:s}".format(str(EE)))
        else :
            if len(source_name)>1 and len(person_name)>1 :
                #gender           = 'unknown' if len(gender)<1 else gender
                #age              = 'unknown' if len(age)<1 else age
                #sentiment        = 'unknown' if len(sentiment)<1 else sentiment
                #smile            = 'unknown' if len(smile)<1 else smile
                #similarity_score = 'unknown' if len(similarity_score)<1 else similarity_score

                result_topic   = "{:s}/{:s}".format(topic_destination, source_name)
                result_payload = "{:s}".format(person_name)
                if len(age)>0 :
                    result_payload += " age={:s}".format(age)
                if "true" in smile :
                    result_payload += " smiling".format(age)
                try :
                    mqc.mqttClient.publish(topic=result_topic, payload=result_payload, qos=0)
                except Exception as EE :
                    logging.warning("Cannot publishing on {:s} : {:s}".format(result_topic, str(EE)))

    # def on_publish(client, userdata, mid) :
    #     print("Callback : on_publish : {:s} {:s} {:s}".format(str(client), str(userdata), str(mid)))

    # ------------ Connection to MQTT ------------
    try :
        mqc = pyclasses.MqttTLS.MQTTConnector(config=conf['mqttbroker'])
        mqc.mqttClient.on_connect    = on_connect
        mqc.mqttClient.on_disconnect = on_disconnect
        mqc.mqttClient.on_message    = on_message
        # mqc.mqttClient.on_publish    = on_publish

        mqc.Connect()
        mqc.mqttClient.loop_forever()
        # mqc.mqttClient.loop_start()
        # mqc.mqttClient.loop_stop()
        mqc.Disconnect()

    except Exception as CEX :
        logging.critical("Exception while MQTT connection : {:s}".format(str(CEX)))
        sys.exit(-1)
