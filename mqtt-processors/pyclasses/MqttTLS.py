# coding=utf-8
__version__ = '190916'

import logging
import ssl
import paho.mqtt.client

# Usage :
# mqc = MQTTConnector(config)
# mqc.mqttClient.on_connect =
# mqc.mqttClient.on_message =
# mqc.mqttClient.on_...
# mqc.Connect()
# mqc.mqttClient.publish(...)
# mqc.Disconnect()
class MQTTConnector :

    def __init__(self, config=None) :
        if config is None :
            config = dict()

        self._myHost         = config.get('host') or '127.0.0.1'
        self._myPort         = config.get('port') or 1883
        self._myUsername     = config.get('username') or ''
        self._myUserpass     = config.get('userpass') or ''
        self._myCAfileForTLS = config.get('ca_pathfile') or ''
        self._myKeepalive    = config.get('keepalive') or 30
        self._mqttClient     = paho.mqtt.client.Client()

    @property
    def mqttClient(self):
        return self._mqttClient

    @mqttClient.setter
    def mqttClient(self, value):
        pass

    def Connect(self):
        retour = True
        if len(self._myCAfileForTLS) > 0 :
            try :
                SSLcontext = ssl.create_default_context()
                SSLcontext.load_verify_locations(cafile=self._myCAfileForTLS)
                self._mqttClient.tls_set_context(context=SSLcontext)
                self._mqttClient.tls_insecure_set(False)
            except Exception as EMQ :
                logging.error("MQTT TLS Exception : {:s}".format(str(EMQ)))
                retour = False

        if len(self._myUsername)> 0 :
            try :
                self._mqttClient.username_pw_set(username=self._myUsername, password=self._myUserpass)
            except Exception as EMQ :
                logging.error("MQTT User Exception : {:s}".format(str(EMQ)))
                retour = False
        try :
            self._mqttClient.connect(host=self._myHost, port=self._myPort, keepalive=self._myKeepalive)
        except Exception as EMQ :
            logging.error("MQTT Connnect Exception : {:s}".format(str(EMQ)))
            retour = False

        return retour

    def Disconnect(self) :
        try :
            self._mqttClient.loop_stop()
            self._mqttClient.disconnect()
        except Exception as EMQ :
            logging.error("MQTT Closing Exception : {:s}".format(str(EMQ)))
            retour = False


