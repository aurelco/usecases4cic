# coding=utf-8
__version__ = '190904'

import re
import collections
import unicodedata
import html
import string

def dict_merge(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (k in dct) and isinstance(dct[k], dict) and isinstance(merge_dct[k], collections.Mapping):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


# -------------------------------- String --------------------------------

def StringMultiReplace(subs, subject) :
    """Simultaneously perform all substitutions of words on the subject string. strMultiReplace([('hi', 'bye'), ('bye', 'hi')], 'hi and bye') -> 'bye and hi'
    For multi-char, better to use : import string ; trans = string.maketrans("ABCDE","12345") ; my_string = my_string.translate(trans)
    """
    pattern = '|'.join('(%s)' % re.escape(p) for p, s in subs)
    substs = [s for p, s in subs]
    replace = lambda m: substs[m.lastindex - 1]
    return re.sub(pattern, replace, subject)

def StringMatchAny(p_patterns, p_original_text) :
    """if StrigMatchAny(['toto','tata','titi'], "blablabla") ... Return True or False
    """
    match_test = [True for match in p_patterns if match in p_original_text]
    return True in match_test

def StringCleanSanitize(input_str,
                     phtmlunescape=True,            # Remove all html tags, unescapes "&" ...
                     pLignesTabsGuillemets=True,    # Remove tabs, end of line, " ` -> '
                     pNormalizeASCII=True,          # Remove accents, euro sign, pound sign, keep $ # & @, normalisation NFKD AND Ascii and char
                     pEnleveSignesSpeciaux=False,   # Keep only -.'+#?!%&*/()=_:; and letters, digits, change { [ -> (
                     pLettreDigitPointTiret=False,  # Keep only -. 0123456789 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
                     pLetterDigitTiretOnly=False,   # Keep letters digits and -
                     pBagOfWords=False):            # Keep letters digits in lower case
    retour = ""
    if input_str is not None and len(input_str) > 0 :
        retour = input_str
        if phtmlunescape :
            retour = re.sub(u"<.*?>", " ", input_str)   # On enleve les balises HTML
            retour = html.unescape(retour)              # on remet les caract normaux

        if pLignesTabsGuillemets :
            retour = retour.replace("""\\n""", """\n""").replace("""\\'""", """'""")
            retour = StringMultiReplace([(r"\'","'"), ('"', "'"), ("’", "'"), ("`", "'"), ('\n', ' '), ('\r', ''), ('\t', ' ')], retour)

        if pNormalizeASCII :   # On fait sauter les accents en les transformant en lettre equivalente
            retour = StringMultiReplace([('€','EUROS'), ('£','POUNDS'), ('°','DEG')], retour)
            retour = (unicodedata.normalize('NFKD', retour).encode('ascii', 'ignore')).decode('utf-8','ignore')
        else: # UTF-8 : on garde plus de trucs
            retour = (retour.encode('utf-8', 'ignore')).decode('utf-8','ignore')  #retour = (unicodedata.normalize('NFKD', retour).encode('utf-8', 'ignore')).decode('utf-8','ignore')

        # --- LES SUIVANTS SE CUMULENT
        if pEnleveSignesSpeciaux :
            trans = str.maketrans("²\\[]{}<>^|~", "2/()()     ")
            retour = retour.translate(trans)
            retour.maketrans("","")
            validFilenameChars="0123456789 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.'+#?!%&*/()=_:;,"
            retour = "".join([c for c in retour if c in validFilenameChars])

        if pLettreDigitPointTiret :
            trans = str.maketrans("²\\[]{}<>^|~!?'%#&/*+:;_()","2/()()     ..            ")
            retour = retour.translate(trans)
            retour.maketrans("","")
            validFilenameChars="0123456789 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-."
            retour = "".join([c for c in retour if c in validFilenameChars])

        if pLetterDigitTiretOnly :
            trans = str.maketrans("²\\[]{}<>^|~!?'%#&/*+:;_().","2                         ")
            retour = retour.translate(trans)
            retour.maketrans("","")
            validFilenameChars="0123456789 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-"
            retour = "".join([c for c in retour if c in validFilenameChars])

        if pBagOfWords :
            trans = str.maketrans("²\\[]{}<>^|~!?'%#&/*+:;_().-","2                          ")
            retour = retour.translate(trans)
            retour.maketrans("","")
            validFilenameChars = " %s%s" % (string.ascii_letters, string.digits)
            retour = "".join([c for c in retour if c in validFilenameChars])
            retour = retour.lower()

        retour = re.sub(u" +", " ", retour).strip() # On enleve les espaces en trop
    return retour

def cleanLangueFr(input_str) :
    return StringCleanSanitize(input_str, phtmlunescape=True, pLignesTabsGuillemets=True,
                                          pNormalizeASCII=False,
                                          pEnleveSignesSpeciaux=False, pLettreDigitPointTiret=False,
                                          pLetterDigitTiretOnly=False, pBagOfWords=False)
def cleanOnlyLetterDigit(input_str) :
    return StringCleanSanitize(input_str, phtmlunescape=True, pLignesTabsGuillemets=True,
                                          pNormalizeASCII=True,
                                          pEnleveSignesSpeciaux=False, pLettreDigitPointTiret=False,
                                          pLetterDigitTiretOnly=True, pBagOfWords=False)
def cleanMax(input_str) :
    return StringCleanSanitize(input_str, phtmlunescape=True, pLignesTabsGuillemets=True,
                                          pNormalizeASCII=True,
                                          pEnleveSignesSpeciaux=False, pLettreDigitPointTiret=False,
                                          pLetterDigitTiretOnly=False, pBagOfWords=True)