# coding=utf-8
__version__ = '190914'

import toml
import os
import sys
import logging
from pathlib import Path
import getpass
import pickle
from cryptography.fernet import Fernet
import base64

class TomlConfig :
    def __init__(self, filename=None):
        self._loaded_from_pickle = False

        if filename is None :
            filename = str(os.path.basename(sys.argv[0]).split(sep='.')[0])
        filename_pickle       = "{:s}.pickle".format(filename)
        filename_conf         = "{:s}.toml".format(filename)
        self._filepath        = Path(filename_conf)
        self._filepath_pickle = Path(filename_pickle)
        tmpK = '{:s}{:s}'.format(filename_pickle, filename_conf)
        tmpK2 = int(32/len(tmpK) + 2)
        tmpK3 = tmpK * tmpK2
        tmpK4 = tmpK3[:32]
        self._fernet          = Fernet(base64.b64encode(tmpK4.encode()))

        self._conf = dict()
        if not self._filepath.exists() :
            # ---- Creation of a TOML file if it doesn't exist
            tmpTexte =  '# TOML : https://github.com/toml-lang/toml  and  https://github.com/avakar/pytoml\n'
            tmpTexte += 'title = "{:s} default created"\n'.format(filename)
            try :
                self._filepath.write_text(data=tmpTexte, encoding='utf-8', errors='backslashreplace')
                self._conf = toml.loads(tmpTexte)
            except Exception as etoml:
                logging.critical("Cannot write default Toml Config file {:s} : {:s}".format(filename_conf, str(etoml)))
                sys.exit(-1)
        elif self._filepath_pickle.exists() :
            try:
                with self._filepath_pickle.open(mode="rb") as fp:
                    chaine = fp.read()
                    self._conf = pickle.loads(self._fernet.decrypt(chaine))
                    self._loaded_from_pickle = True
            except Exception as epick:
                logging.critical("Cannot read Pickle Config file {:s} : {:s}".format(filename_pickle, str(epick)))
        else :
            # ---- Reading the TOML file for the configuration
            try :
                toml_text = self._filepath.read_text(encoding='utf-8', errors='backslashreplace')
                self._conf = toml.loads(toml_text)
            except Exception as etoml:
                logging.critical("Cannot read Toml Config file {:s} : {:s}".format(filename_conf, str(etoml)))
                sys.exit(-1)

    @property
    def conf(self):
        return self._conf

    @conf.setter
    def conf(self, value):
        self._conf = value

    def prompt_for_values(self, propose_pickle=True):
        if self._loaded_from_pickle :
            print("Config loaded from pickle")
        else :
            # reccursive function
            def recur_prompt(dict_field, name_str) :
                thekeys = dict_field.keys()
                #for thekey in sorted(thekeys) :
                for thekey in thekeys :
                    if type(dict_field[thekey]) is dict :
                        if len(name_str)>0 :
                            recur_prompt(dict_field=dict_field[thekey], name_str="{:s}[{:s}]".format(name_str, thekey))
                        else :
                            recur_prompt(dict_field=dict_field[thekey], name_str="[{:s}]".format(thekey))
                    elif type(dict_field[thekey]) is str :
                        if dict_field[thekey].startswith("TO_PROMPT") :
                            if len(name_str) > 0:
                                name_str2 = "{:s}[{:s}]".format(name_str, thekey)
                            else :
                                name_str2 = "[{:s}]".format(thekey)
                            fTmp, fType, fDefault = dict_field[thekey].split(";")
                            if fType not in [ 'int' , 'float', 'str' ] :
                                fType = 'str'
                            fQuestion = "\nConfig : Enter value for {:s} ({:s} , default={:s}) : ".format(name_str2, fType, str(fDefault))
                            if dict_field[thekey].startswith("TO_PROMPT_SECRET") :
                                fInput = getpass.getpass(fQuestion)
                            else :
                                fInput = input(fQuestion)
                            fInput2 = fDefault
                            if len(fInput) > 0 :
                                fInput2 = fInput
                            if fType == 'int' :
                                fInput2 = int(fInput2)
                            elif fType == 'float' :
                                fInput2 = float(fInput2)
                            else :
                                fInput2 = str(fInput2)
                            dict_field[thekey] = fInput2
            try :
                recur_prompt(self._conf, "")
            except Exception as recur:
                logging.critical("Exception while prompting for config : {:s}".format(str(recur)))
                sys.exit(-1)

        if propose_pickle and not self._loaded_from_pickle :
            qPic = input("\nDo you want to save locally (y/n, default=N) ?")
            if qPic.lower() in ['y', 'yes'] :
                self.pickle_to_file()

    def pickle_to_file(self):
        if not self._loaded_from_pickle :
            try:
                with self._filepath_pickle.open(mode="wb") as fp :
                    chaine = pickle.dumps(self._conf, protocol=pickle.HIGHEST_PROTOCOL)
                    fp.write(self._fernet.encrypt(chaine))

            except Exception as epick:
                logging.error("Cannot write pickle file : {:s}".format(str(epick)))

